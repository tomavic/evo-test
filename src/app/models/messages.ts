export interface Message {
  id: number;
  text: string;
  createdDate: string;
  modifiedDate: string;
  createdByUserId: string;
  createdByUserName: string;
}