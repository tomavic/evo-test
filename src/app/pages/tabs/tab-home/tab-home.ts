import { Component, AfterViewInit } from '@angular/core';
import { Message } from 'src/app/models/messages';
import { MessagingService } from 'src/app/providers';

@Component({
  selector: 'app-tabs-home',
  templateUrl: 'tab-home.html',
  styleUrls: ['tab-home.scss']
})
export class HomeTabPage implements AfterViewInit {

  feMessages: boolean = false;
  latestChatList: Message;
  chatHistoryThreadList: Array<Message> = [];


  constructor(private messageService: MessagingService) {

    this.getLatestMessage()
  }

  ngAfterViewInit() {

    
  }


  getLatestMessage() {
    this.chatHistoryThreadList = this.messageService.getAll() || [];
    console.log("chatHistoryThreadList >> ", this.chatHistoryThreadList);
    if (this.chatHistoryThreadList.length == 0) {
      this.latestChatList = {  
        id: 8,
        text: "",
        createdDate: "",
        modifiedDate: "",
        createdByUserId: "",
        createdByUserName: ""
      };
      this.feMessages = false;
    } else {
      this.feMessages = true;
      this.latestChatList = this.chatHistoryThreadList[this.chatHistoryThreadList.length-1];
    }
    console.log("latestChatList >> ", this.latestChatList);
  }

  
}
