import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { APP_MODULES, APP_SERVICES } from './app.imports';
import { HomeTabPage } from './pages/tabs/tab-home/tab-home';
import { MapTabPage } from './pages/tabs/tab-map/tab-map';
import { ChatTabPage } from './pages/tabs/tab-chat/tab-chat';

const ComponentsList = [
  HomeTabPage,
  MapTabPage,
  ChatTabPage
]


@NgModule({
  declarations: [AppComponent, ComponentsList],
  imports: [APP_MODULES],
  providers: [APP_SERVICES],
  bootstrap: [AppComponent]
})
export class AppModule { }
