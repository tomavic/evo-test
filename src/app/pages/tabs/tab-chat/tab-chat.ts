import { Component, AfterViewInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { MessagingService } from 'src/app/providers';
import { Message } from 'src/app/models/messages';

@Component({
  selector: 'app-tabs-chat',
  templateUrl: 'tab-chat.html',
  styleUrls: ['tab-chat.scss']
})
export class ChatTabPage implements AfterViewInit, AfterViewChecked {

  @ViewChild(ElementRef) scrollMe: ElementRef;
  chatHistoryThreadList: Array<Message>;
  textMessage: string // NgModel

  constructor(private messageService: MessagingService) {
    this.getHistoryMsgs() 
  }


  ngAfterViewInit() {
    this.getHistoryMsgs()
  }


  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    let element = document.getElementById("scrollMe");
    // element.scrollIntoView();
    // this.scrollMe.nativeElement.scrollTop = this.scrollMe.nativeElement.scrollHeight;               
  }

  getHistoryMsgs() {
    this.chatHistoryThreadList = this.messageService.getAll();
    console.log("chatHistoryThreadList >> ",this.chatHistoryThreadList);
  }


  reset() {
    this.messageService.reset();
    this.getHistoryMsgs();
  }

  async sendMessage() {
    
    let data: Message = {
      "id": Math.floor(Math.random() * 10) + 1,
      "text": this.textMessage,
      "createdDate": new Date().toISOString(),
      "createdByUserId": "7",
      "modifiedDate": new Date().toISOString(),
      "createdByUserName": "Hamada"
    }
    await this.messageService.addMessage(data)
    .then((res) => {
      this.textMessage = "";
      this.getHistoryMsgs();
      this.scrollToBottom()
    })
    .catch(err => console.error(err));
  }


}
