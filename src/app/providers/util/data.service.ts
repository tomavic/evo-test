import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from 'src/app/models/messages';

@Injectable()
export class DataService {

  baseUrl: string = "assets/mocks/";

  constructor(private httpClient : HttpClient) { 

  }

  getThreadHistoryMessages() {
    let req = this.httpClient.get(this.baseUrl + 'messages.json');
    req.subscribe((res: any) => {
      localStorage.setItem("app_thread_msg_list", JSON.stringify(res.data));
      console.log("Array set on storage ", JSON.parse(localStorage.getItem("app_thread_msg_list")));
    });
    return req;
  }

  addMessage(msg: Message) {
    return this.httpClient.post(this.baseUrl + 'messages.json', msg);
  }

}
