/** 

  ░░░░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ 
  ░░░░░░░░░░█░░░░░░▀█▄▀▄▀██████░░░▀█▄▀▄▀██████ 
  ░░░░░░░░ ░░░░░░░░░░▀█▄█▄███▀░░░░░░▀█▄█▄███▀░

  @author TOoma Besheer™ | 2019-2020 © 

*/

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatGridListModule, MatTabsModule, MatButtonModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatInputModule, MatListModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { MessagingService, DataService } from './providers';
import { FormsModule } from '@angular/forms';


export const APP_MODULES = [
  BrowserModule,
  FormsModule,
  BrowserAnimationsModule,

  // `Angular Material` Modules
  MatButtonModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule,


  // Routing modules
  AppRoutingModule,


  // modules
  HttpClientModule
]



export const APP_SERVICES = [
  MessagingService,
  DataService
]