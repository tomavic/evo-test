import { Component, ViewChild } from '@angular/core';

import { MatTabGroup } from '@angular/material';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MessagingService } from './providers';



@Component({
  selector: 'test-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  tabTitle: string;
  activeLinkIndex = -1;
  @ViewChild('tabGroup') tabGroup: MatTabGroup;

  // tabs
  tabs = [
    {
      title: "Home",
      id: 0,
      icon: "home",
      content: "Content Home",
      link: './home',
    },
    {
      title: "Map",
      id: 1,
      icon: "map",
      content: "Content Maps",
      link: './map',
    },
    {
      title: "Chat",
      id: 2,
      icon: "chat_bubble_outline",
      content: "Content Chats",
      link: './chat',
    }
  ];

  constructor(
    private messageService: MessagingService,
    private router: Router,
    private route: ActivatedRoute) {

  }

  ngOnInit(): void {

    this.messageService.init();

    // adjust routing
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    ).subscribe((e: NavigationEnd) => {
      this.activeLinkIndex = this.tabs.indexOf(this.tabs.find(tab => tab.link === '.' + this.router.url));
      this.tabs.forEach(element => {
        if(element.id == this.activeLinkIndex) this.tabTitle = element.title;
      });
    });
  }



}
