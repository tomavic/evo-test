import { Injectable } from '@angular/core';
import { Message } from 'src/app/models/messages';
import { DataService } from '../util/data.service';



@Injectable()
export class MessagingService {
 
  threadMessagesList: Array<Message> = [];
  lastMessageInThread: Message;

  constructor(public dataService: DataService) {
    this.init();
  }


  init() {
    this.dataService.getThreadHistoryMessages().subscribe((res: any) => {
      // this line to ensure it has already in storage
      localStorage.setItem("app_thread_msg_list", JSON.stringify(res.data));
      this.threadMessagesList = JSON.parse(localStorage.getItem("app_thread_msg_list"));
      this.lastMessageInThread = res.data[res.data.length -1];

      console.log("this.threadMessagesList ", this.threadMessagesList);
      console.log("this.lastMessageInThread ", this.lastMessageInThread);
      
    });
  }

  getAll(): Array<Message> {
    return JSON.parse(localStorage.getItem("app_thread_msg_list"));
  } 

  reset() {
    localStorage.removeItem("app_thread_msg_list");
    localStorage.setItem("app_thread_msg_list", JSON.stringify([]));
    this.threadMessagesList = [];
  }

  setAll(list: Array<Message>) {
    localStorage.setItem("app_thread_msg_list", JSON.stringify(list));
  }
  

  addMessage(message: Message): Promise<any> {
    this.threadMessagesList.push(message);
    this.setAll(this.threadMessagesList);
    return new Promise((resolve, reject) => {
      resolve("success sending");
      reject("failed sending")
    })
  }

}