import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeTabPage } from './pages/tabs/tab-home/tab-home';
import { MapTabPage } from './pages/tabs/tab-map/tab-map';
import { ChatTabPage } from './pages/tabs/tab-chat/tab-chat';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component:  HomeTabPage },
  { path: 'map', component:  MapTabPage},
  { path: 'chat', component: ChatTabPage},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }